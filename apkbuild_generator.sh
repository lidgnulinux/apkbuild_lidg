yad --form --title="APKBUILD Generator." \
	--field="Package Name" \
	--field="Package Version" \
	--field="Package Release" \
	--field="Package Description" \
	--field="URL" \
	--field="Arch" \
	--field="License" \
	--field="Options" \
	| tr -s '|' '\n' > input.txt

pkgname=$(sed -n 1p input.txt)
pv=$(sed -n 2p input.txt)
pr=$(sed -n 3p input.txt)
pkgdesc=$(sed -n 4p input.txt)
url=$(sed -n 5p input.txt)
arch=$(sed -n 6p input.txt)
license=$(sed -n 7p input.txt)
opt=$(sed -n 8p input.txt)

cat >APKBUILD_generated<<__EOF__
# Contributor:
# Maintainer:
pkgname=$pkgname
pkgver=$pv
pkgrel=$pr
pkgdesc="$pkgdesc"
url="$url"
arch="$arch"
license="$license"
depends="$depends"
depends_dev=""
makedepends="$makedepends"
checkdepends="$checkdepends"
install="$install"
subpackages="\$pkgname-dev \$pkgname-doc"
options="$opt"
source="$source"

builddir=""
prepare() {
}

build() {
}

package() {
}

sha512sums=""
__EOF__
