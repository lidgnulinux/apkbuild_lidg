# What are these ?

These are some custom APKBUILDs for software I found interesting. I will make a plan to maintain it (I have a lot of free time...).

- [swiv](https://github.com/ShaqeelAhmad/swiv) (fork of sxiv for wayland).
- [neovim-qt](https://github.com/equalsraf/neovim-qt) (Lightweight cross-platform Neovim GUI written in C++ with Qt).
- [ksnip](https://github.com/ksnip/ksnip) (ksnip the cross-platform screenshot and annotation tool).
- [bleachbit](https://github.com/bleachbit/bleachbit) (Deletes unneeded files to free disk space and maintain privacy).
- [libxfce4windowing](https://gitlab.xfce.org/xfce/libxfce4windowing) (Windowing concept abstraction library for X11 and Wayland).
- [xfce4-panel + wayland support](https://gitlab.xfce.org/xfce/xfce4-panel) (Panel for the Xfce desktop environment).
- [wlroots without Xwayland](https://gitlab.freedesktop.org/wlroots/wlroots) (Modular Wayland compositor library without Xwayland).
- [wayst](https://github.com/91861/wayst) (A simple terminal emulator).
- [sway without Xwayland](https://github.com/swaywm/sway) (i3-compatible window manager for Wayland).
- [dtao](https://github.com/djpohly/dtao) (dzen for wayland).
- [sway-goodies](https://gitlab.com/lidgnulinux/sway-goodies) (Some (maybe) usable scripts for SwayWM).
- [transparent-shell-theme](https://gitlab.com/lidgnulinux/transparent-shell-theme) (Simple transparent shell theme for gnome shell).
- [vbeterm](https://github.com/vincentbernat/vbeterm) (Simple custom terminal based on VTE).
- [mdo](https://github.com/eyalev/mdo) (Terminal markdown viewer).
- [mpvc](https://github.com/lwilletts/mpvc) (An mpc-like control interface for mpv).
- [dwl + vanity gaps](https://codeberg.org/dwl/dwl) (dwm for wayland with vanity gaps applied).
- [dwl + ipc](https://codeberg.org/dwl/dwl) (dwm for wayland with ipc patch applied).
- [dwlmsg](https://codeberg.org/notchoc/dwlmsg) (Tool to send ipc messages to dwl).
- [stardict-eng-ind](http://download.huzheng.org/Quick/) (English-Indonesia dictionary for stardict).
- [SRM](https://github.com/CuarzoSoftware/SRM) (Simple Rendering Manager).
- [vim-wayland-clipboard](https://github.com/jasonccox/vim-wayland-clipboard) (Integrate Vim's '+' register with the Wayland system clipboard).
- [gtk4-layer-shell](https://github.com/wmww/gtk4-layer-shell) (A library to create panels and other desktop components for Wayland using the Layer Shell protocol and GTK4).
- other (coming soon).

# Notes

1. If you want to try to build, make sure you are using alpine (obviously) and your user is added to `abuild` group. 

    ```
    # addgroup <yourusername> abuild
    ```

1. Make sure you read the whole content of APKBUILD (e.g I use `nvim` as editor for editing config.def.h on dwl APKBUILD, you may have different editor) !
1. I'm using alpine 3.19.

# How to build package using APKBUILD ?

Here are some steps if you want to build package using APKBUILD :

1. Generate signing keys ! We can use `abuild-keygen`.

    ```
    $ abuild-keygen
    ```

1. Copy the signing keys to /etc/apk/keys/ !

    ```
    $ sudo cp -r /home/lidg/.abuild/<your-signing-keys.rsa.pub /etc/apk/keys
    ```

1. Get / make APKBUILD ! You can use my APKBUILDs as example (obviously), then you can copy it to an empty directory.

1. Build the package ! We can use this command.

    ```
    $ abuild -r
    ```

    You can check the result at ~/packages.

1. Install the package ! We can install the package using this command.

    ```
    $ sudo apk add /home/$USER/packages/$startdir/x86_64/example_package.apk
    ```

    `$startdir` is parent directory name (not fullpath) where the APKBUILD is stored.
