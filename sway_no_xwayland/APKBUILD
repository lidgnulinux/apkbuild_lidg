# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Contributor: Antoine Fontaine <antoine.fontaine@epfl.ch>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=sway
pkgver=1.9.0
pkgrel=0
pkgdesc="i3-compatible window manager for Wayland"
url="https://swaywm.org/"
license="MIT"
arch="all"
makedepends="
	basu-dev
	cairo-dev
	eudev-dev
	gdk-pixbuf-dev
	json-c-dev
	libcap-utils
	libevdev-dev
	libinput-dev
	libxkbcommon-dev
	linux-pam-dev
	meson
	ninja
	pango-dev
	pcre2-dev
	scdoc
	wayland-dev
	wayland-protocols
	wlroots-dev
	"
options="!check"
source="https://github.com/swaywm/sway/releases/download/1.9/sway-1.9.tar.gz"
builddir="$srcdir/$pkgname-1.9"

build() {
	cd "$builddir"
	C_INCLUDE_PATH=/usr/include/ \
		PKG_CONFIG_PATH=/usr/lib/pkgconfig/ \
		LD_LIBRARY_PATH=/usr/lib/ \
		abuild-meson \
		-Db_lto=true \
		-Dsd-bus-provider=basu \
		-Dbash-completions=false \
		-Dzsh-completions=false \
		-Dfish-completions=false \
		-Ddefault-wallpaper=false \
		-Dman-pages=disabled \
		-Dxwayland=disabled \
		. output

	C_INCLUDE_PATH=/usr/include/ \
		PKG_CONFIG_PATH=/usr/lib/pkgconfig/ \
		LD_LIBRARY_PATH=/usr/lib/ \
		meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="1d2a47bb8b838573a32f3719a7329fd744119c2c7efc5e5a4168b2bacfb09a3901a569177e5e10c129141fafe00e823ab78c04b76b502d23caa7621bbccd5919  sway-1.9.tar.gz"
